import os,shutil,re,sys

def cd(path):
    if path=="..":
        os.chdir(os.pardir)
    elif check_path(path):
            os.chdir(path)



def ls():
    for f in os.listdir(os.curdir):
        print(f)

def dir():
    ls()

def cat_writer(path):
     with open(path,'w') as f:
         st = raw_input()
         while st!=":q":
             f.write(st)
             st = raw_input()

def cat_reader(path):
    print(path)
    if check_path(path):
        with open(path) as f:
            print f.read()

def cat(st):
    if st[0]=='>':
        print(st[2:])
        if len(st)>1:
            path = st.split()[1]
            cat_writer(path)
        else:
            print("Comand error. cat > \"file name\"")
    else:
        cat_reader(st)

def rm(path):
    if check_path(path):
        os.remove(path)

def check_path(path):
    if not os.path.exists(path):
        print("Path "+path+" not exists")
        return False
    return True

st=raw_input(os.path.abspath(os.curdir))
while st!="exit":
    comand = st.split()
    if len(st)>1:
        if comand[0]=="cd":
            cd(comand[1])
        elif comand[0]=="ls":
            ls()
        elif comand[0]=="rm":
            rm(comand[1])
        elif comand[0]=="dir":
            dir()
        elif comand[0]=="cat":
            cat(" ".join(comand[1:]))
    st = raw_input(os.path.abspath(os.curdir))
