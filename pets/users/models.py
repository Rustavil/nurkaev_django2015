from django.db import models

# Create your models here.
class Persona(models.Model):
    name = models.CharField(max_length=50)
    age = models.IntegerField()
    email = models.EmailField()


class Msg(models.Model):
    user_send = models.ForeignKey(Persona,related_name='sender')
    user_geting = models.ForeignKey(Persona,related_name='geting')
    text = models.CharField(max_length=4096)

class Advert(models.Model):
    persona = models.ForeignKey(Persona)
    title = models.CharField(max_length=255)
    text = models.CharField(max_length=4096)
    actual = models.IntegerField(default=5)