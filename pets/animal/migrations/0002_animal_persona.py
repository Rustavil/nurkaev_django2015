# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
        ('animal', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='animal',
            name='persona',
            field=models.ManyToManyField(to='users.Persona'),
        ),
    ]
