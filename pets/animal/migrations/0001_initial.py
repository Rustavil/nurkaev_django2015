# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Animal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=10, choices=[(b'L', b'Lucky'), (b'S', b'Seeking home')])),
                ('name', models.CharField(max_length=50)),
                ('gender', models.CharField(max_length=10, choices=[(b'F', b'female'), (b'M', b'male')])),
                ('age', models.IntegerField()),
                ('description', models.CharField(max_length=4096)),
            ],
        ),
        migrations.CreateModel(
            name='Breed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=4096)),
            ],
        ),
        migrations.CreateModel(
            name='Type',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10)),
                ('description', models.CharField(max_length=4096)),
            ],
        ),
        migrations.AddField(
            model_name='animal',
            name='type',
            field=models.ForeignKey(to='animal.Breed'),
        ),
    ]
