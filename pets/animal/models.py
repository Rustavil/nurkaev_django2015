from django.db import models
from users.models import Persona
# Create your models here.
class Type(models.Model):
    name = models.CharField(max_length=10)
    description = models.CharField(max_length=4096)


class Breed(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=4096)


class Animal(models.Model):
    persona = models.ManyToManyField(Persona)
    status = models.CharField(max_length=10, choices={("S","Seeking home"),("L","Lucky")})
    name = models.CharField(max_length=50)
    gender = models.CharField(max_length=10, choices={('M','male'),('F','female')})
    age = models.IntegerField()
    type = models.ForeignKey(Breed)
    description = models.CharField(max_length=4096)