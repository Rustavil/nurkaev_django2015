from django.db import models

# Create your models here.
class Shelter(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=4096)

class Review(models.Model):
    shelter = models.ForeignKey(Shelter)
    text = models.CharField(max_length=2048)