from django.db import models

# Create your models here.
class New(models.Model):
    title = models.CharField(max_length=1024)
    text = models.CharField(max_length=4096)


class Comment(models.Model):
    new = models.ForeignKey(New)
    text = models.CharField(max_length=4096)
