# Create your views here.
from django.shortcuts import render_to_response
from django.views.generic import ListView
from pets.forms import PetForm
from pets.models import Breed


def show_pet(request, id):
    return render_to_response('pets/show_pet.html')


def add_pet(request):
    form = PetForm()
    if request.method == "POST":
        form = PetForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    return render_to_response('pets/add_pet.html', {'form': form})


class BreedsList(ListView):
    template_name = "pets/breeds.html"

    def get_queryset(self):
        return Breed.objects.all()

