from django.contrib import admin
from pets.models import Pet, TypeOfPet, Breed

# Register your models here.
admin.site.register(TypeOfPet)
admin.site.register(Breed)
admin.site.register(Pet)
