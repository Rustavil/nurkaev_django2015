from django.db import models
from shelters.models import Shelter
from users.models import Profile

# Create your models here.


class TypeOfPet(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Breed(models.Model):
    type = models.ForeignKey(TypeOfPet)
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=4096)

    def __str__(self):
        return self.name


GENDER = (
    ('F', 'Female'),
    ('M', 'Male')
)


class Pet(models.Model):
    breed = models.ForeignKey(Breed)
    user = models.ForeignKey(Profile)
    photo = models.ImageField()
    shelter = models.ForeignKey(Shelter)
    nickname = models.CharField(max_length=50)
    birthday = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, choices=GENDER)

    def __str__(self):
        return self.breed.type.name+": "+self.nickname
