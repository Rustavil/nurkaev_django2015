from django.conf.urls import url
from pets.views import BreedsList

urlpatterns = [
    url(r'breeds^$', BreedsList.as_view()),
    url(r'^(?P<id>[0-9]+)/$', 'pets.views.show_pet', name='show'),
    url(r'^add/$', 'pets.views.add_pet', name='add')
]
