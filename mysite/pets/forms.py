from django.forms import ModelForm
from pets.models import Pet


class PetForm(ModelForm):
    class Meta:
        model = Pet
        fields = ['breed', 'nickname', 'birthday', 'gender', 'photo']
