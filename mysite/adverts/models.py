from django.db import models


# Create your models here.
class Advert(models.Model):
    img = models.ImageField(width_field=900, height_field=500)
    title = models.CharField(max_length=50)
    text = models.CharField(max_length=512)
