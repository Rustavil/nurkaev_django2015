from django.db import models


# Create your models here.
class News(models.Model):
    image = models.ImageField()
    date = models.DateField(auto_now=True)
    title = models.CharField(max_length=255)
    text = models.CharField(max_length=4096)

