from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'news.views.show', name='show'),
    url(r'^(?P<id>[0-9]+)/$', 'news.views.details', name='details')
]
