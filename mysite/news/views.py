from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response


def show(request):
    return render_to_response('news/show.html')


def details(request, id):
    return render_to_response('news/details.html')