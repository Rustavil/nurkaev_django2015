from django import forms
from shelters.models import Shelter


class ShelterForm(forms.ModelForm):
    class Meta:
        model = Shelter
        fields = ['name', 'photo', 'description', 'address']
