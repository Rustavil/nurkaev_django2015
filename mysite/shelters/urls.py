from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'shelters.views.show', name='show'),
    url(r'^(?P<id>[0-9]+)/$', 'shelters.views.detail', name='detail'),
    url(r'^add/$', 'shelters.views.add', name='add')
]