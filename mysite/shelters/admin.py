from django.contrib import admin

# Register your models here.
from shelters.models import Shelter

admin.site.register(Shelter)