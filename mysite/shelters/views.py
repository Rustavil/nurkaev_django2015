from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response
from shelters.form import ShelterForm


def show(request):
    return render_to_response('shelters/show.html')


def detail(request, id):
    return render_to_response('shelters/detail.html')


def add(request):
    form = ShelterForm()
    if request.method == "POST":
        form = ShelterForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
    return render_to_response('shelters/add.html')