from django.db import models

# Create your models here.


class Shelter(models.Model):
    name = models.CharField(max_length=10)
    photo = models.ImageField()
    description = models.CharField(max_length=4096)
    address = models.CharField(max_length=1024)
