# -*- coding: utf-8 -*-

import datetime
from django import forms


GENDER = (
    ('F', 'Female'),
    ('M', 'Male')
)


class LoginForm(forms.Form):
    login = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class RegistrationForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u"Логин")
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}), label=u"Email")
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u"Имя")
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u"Фамилия")
    photo = forms.ImageField(widget=forms.FileInput(attrs={'class': 'form-control'}), label=u"Фотография")
    birthday = forms.DateField(initial=datetime.date.today, widget=forms.DateInput(attrs={'class': 'form-control'}), label=u"Дата рождения")
    gender = forms.ChoiceField(choices=GENDER, widget=forms.Select(attrs={'class': 'form-control'}), label=u"Пол")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label=u"Пароль")
    rep_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label=u"Повтроите пароль")


class UpdateForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u"Имя")
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=u"Фамилия")
    photo = forms.ImageField(widget=forms.FileInput(attrs={'class': 'form-control'}), label=u"Фотография")
    birthday = forms.DateField(initial=datetime.date.today, widget=forms.DateInput(attrs={'class': 'form-control'}), label=u"Дата рождения")
    gender = forms.ChoiceField(choices=GENDER, widget=forms.Select(attrs={'class': 'form-control'}), label=u"Пол")
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label=u"Пароль")
    rep_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}), label=u"Повтроите пароль")
