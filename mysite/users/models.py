# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

GENDER = (
    ('F', 'Female'),
    ('M', 'Male')
)


# Create your models here.
class Profile(models.Model):
    photo = models.ImageField()
    user = models.OneToOneField(User)
    birthday = models.DateField(auto_now_add=True)
    gender = models.CharField(max_length=1, choices=GENDER)

    def __str__(self):
        return self.user.username


