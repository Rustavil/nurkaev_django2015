# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.shortcuts import render
from users.forms import LoginForm, RegistrationForm, UpdateForm
from django.contrib.auth import authenticate, login as auth_login, logout as logout_user
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from models import Profile


# Create your views here.
def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse("mysite.views.home"))
    else:
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                user = authenticate(
                    username=form.cleaned_data["login"],
                    password=form.cleaned_data["password"]
                )
                if user is not None:
                    auth_login(request, user)
                    if "next" in request.GET:
                        return HttpResponseRedirect(request.GET["next"])
                    else:
                        return HttpResponseRedirect(reverse("mysite.views.home"))

            return render(request, "users/login.html", {"form": form})
        else:

            form = LoginForm()
            return render(request, "users/login.html", {"form": form})


@login_required(login_url="/users/login")
def logout(request):
    logout_user(request)
    if "next" in request.GET:
        return HttpResponseRedirect(request.GET["next"])
    else:
        return HttpResponseRedirect(reverse("users.views.my"))


def register(request):
    if request.method == "POST":
        form = RegistrationForm(request.POST)
        if form.is_valid():
            profile = Profile(
                user=User.objects.create_user(
                    username=form.cleaned_data["username"],
                    email=form.cleaned_data["email"],
                    password=form.cleaned_data["password"],
                    first_name=form.cleaned_data["first_name"],
                    last_name=form.cleaned_data["last_name"]
                ),
                photo=form.cleaned_data["photo"],
                birthday=form.cleaned_data["birthday"],
                gender=form.cleaned_data["gender"]
            ).save()
            return render(request, 'users/success.html')
        else:
            print(form.is_valid())
            return render(request, "users/registration.html", {"form": form})

    else:
        form = RegistrationForm()
        return render(request, "users/registration.html", {"form": form})


def profile(request, id):
    profile = Profile.objects.get(pk=id)
    isEdite = request.user.id == id
    return render(request, 'users/profile.html', {"profile": profile, "isEdite": isEdite})


@login_required(login_url="/users/login")
def my(request):
    if request.POST:
        form = UpdateForm(request.POST)
        if form.is_valid():
            profile = Profile(
                user=User.objects.create_user(
                    username=form.cleaned_data["username"],
                    email=form.cleaned_data["email"],
                    password=form.cleaned_data['password'],
                    first_name=form.cleaned_data["first_name"],
                    last_name=form.cleaned_data["last_name"]
                )
            ).save()
        return HttpResponseRedirect(reverse("my"))
    else:
        profile = Profile.objects.get(user=request.user)
        form = UpdateForm()
        form.fields['first_name'].initial = profile.user.first_name
        form.fields['last_name'].initial = profile.user.last_name
        form.fields['gender'].initial = profile.gender
        form.fields['birthday'].initial = profile.birthday
        return render(request, 'users/my_profile.html', {"profile": profile, "form":form})
