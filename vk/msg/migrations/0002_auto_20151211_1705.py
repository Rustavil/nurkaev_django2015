# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('msg', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='msg',
            name='date',
            field=models.DateField(default=datetime.datetime(2015, 12, 11, 17, 5, 14, 919000)),
        ),
    ]
