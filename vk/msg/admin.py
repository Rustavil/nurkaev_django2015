from django.contrib import admin

# Register your models here.
from msg.models import Msg, Dialog

admin.site.register(Msg)
admin.site.register(Dialog)