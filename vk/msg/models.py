from datetime import datetime
from django.db import models


# Create your models here.


class Dialog(models.Model):
    name = models.CharField(max_length=256)

    def last_msg(self):
        return self.msg_set.order_by('-date').first()

    def count_acc(self):
        return len(self.account_set.all())

    def __str__(self):
        return self.name


from account.models import Account


class Msg(models.Model):
    user = models.ForeignKey(Account)
    dialog = models.ForeignKey(Dialog)
    date = models.DateField(default=datetime.now())
    text = models.CharField(max_length=2048)

    def __str__(self):
        return self.dialog.name+"-"+self.text

