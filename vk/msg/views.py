from django.shortcuts import render

# Create your views here.
from account.models import Account


def im(request):
    account = Account.objects.get(user=request.user)
    dialogs = account.dialogs.all()
    return render(request,'msg/im.html', {'account': account,'dialogs': dialogs})