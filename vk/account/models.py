from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from msg.models import Dialog


class Account(models.Model):
    user = models.ForeignKey(User)
    dialogs = models.ManyToManyField(Dialog, blank=True)
    frends = models.ManyToManyField("self", blank=True)

    def __str__(self):
        return self.user.username
