# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20151211_1659'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='dialogs',
            field=models.ManyToManyField(to='msg.Dialog', blank=True),
        ),
        migrations.AlterField(
            model_name='account',
            name='frends',
            field=models.ManyToManyField(related_name='_frends_+', to='account.Account', blank=True),
        ),
    ]
