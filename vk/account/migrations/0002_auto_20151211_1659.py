# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('msg', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='dialogs',
            field=models.ManyToManyField(to='msg.Dialog'),
        ),
        migrations.AddField(
            model_name='account',
            name='frends',
            field=models.ManyToManyField(related_name='_frends_+', to='account.Account'),
        ),
        migrations.AddField(
            model_name='account',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
